package com.google.formation.service;

import com.google.formation.bean.Book;
import com.google.formation.dao.BookDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookServiceApi {
    @Autowired
    private BookDao bookDao;

    @Override
    public Integer count() {
        return bookDao.count();
    }

    @Override
    public Book findBookById(Integer id) {
        return bookDao.findBookById(id);
    }

    @Override
    public List<Book> findBooks() {
        return bookDao.findBooks();
    }

    @Override
    public Integer addBook(Book book) {
        return bookDao.addBook(book);
    }

    @Override
    public Integer updateBook(Integer id, Book book) {
        return bookDao.updateBook(id, book);
    }

    @Override
    public void deleteBook(Integer id) {
        bookDao.deleteBook(id);
    }
}
