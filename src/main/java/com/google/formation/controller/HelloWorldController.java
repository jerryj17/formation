package com.google.formation.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {
    @GetMapping(path = "/toto", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> helloWorld() {
        return ResponseEntity.ok("Hello world !");
    }

    @GetMapping(path = "/titi", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> titi() {
        return ResponseEntity.ok("TITI !");
    }
}
